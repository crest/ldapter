
import logging
import random
import sys

from ldapter import CacheListener
from ldapter import Directory, Scope
from ldapter import RedisDirectoryCache
from ldapter import inet_org_person_schema


def print_entry(dn, entry):
    print("dn:", dn)
    for k, v in entry.items():
        print(f"{k}: {v}")
    print()


def person_search_by_uid(directory: Directory):
    base = "dc=example,dc=org"
    search_filter = "uid=fletcher"
    for dn, entry in directory.fetch_all(base, Scope.SUBTREE, search_filter):
        print_entry(dn, entry)


def person_search_by_name(directory: Directory):
    people = directory.fetch_all(
        "dc=example,dc=org", Scope.SUBTREE, "(&(sn=Smith)(givenName=Mary*))")
    if len(people) == 0:
        print("nobody named Mary Smith?!")
        return

    # pick one at random
    selected = random.randrange(0, len(people))

    print(f"found {len(people)} people named Mary Smith")
    for i, (dn, person) in enumerate(people):
        print(f"  {person['cn']} {person['employeeNumber']} {dn} {'(selected)' if i == selected else ''}")
    print()

    # get the DN from the selected person's tuple
    selected_person = people[selected]
    dn = selected_person[0]

    # this fetch will be always be satisfied from cache
    person = directory.fetch_one(dn, Scope.BASE)

    print_entry(*person)


class LoggingListener(CacheListener):

    def on_entry_miss(self, dn):
        print("---- entry cache miss:", dn)

    def on_entry_hit(self, dn, entry):
        print("---- entry cache hit: ", dn)

    def on_search_miss(self, base, scope, search_filter):
        print("---- search cache miss:", base, scope, search_filter)

    def on_search_hit(self, base, scope, search_filter, dns):
        print("---- search cache hit: ", base, scope, search_filter)


if __name__ == "__main__":

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    cache = RedisDirectoryCache(host="localhost", port=56379, db=0, cache_listener=LoggingListener())

    directory = Directory(inet_org_person_schema(),
                          cache=cache,
                          start_tls=False,
                          uri="ldap://localhost:59389",
                          simple_bind=True,
                          bind_dn="cn=admin,dc=example,dc=org",
                          bind_password="admin")

    person_search_by_uid(directory)
    person_search_by_name(directory)
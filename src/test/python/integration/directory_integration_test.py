import os
from unittest.mock import Mock
from ldapter import Directory
from ldapter import RedisDirectoryCache
from ldapter import DistinguishedName
from ldapter import Scope
from ldapter import inet_org_person_schema

import ldap
from callee import Captor, Dict, List, Int
from pytest import fixture

BIND_DN = os.environ.get("BIND_DN", "cn=admin,dc=example,dc=org")
BIND_PASSWORD = os.environ.get("BIND_PASSWORD", "admin")

REDIS_HOST = os.environ.get("REDIS_HOST", "localhost")
REDIS_PORT = os.environ.get("REDIS_PORT", "56379")
LDAP_HOST = os.environ.get("LDAP_HOST", "localhost")
LDAP_PORT = os.environ.get("LDAP_PORT", "59389")


@fixture
def cache():
    cache = RedisDirectoryCache(host=REDIS_HOST, port=int(REDIS_PORT), cache_listener=Mock())
    cache.clear()
    return cache


@fixture
def directory(cache):
    directory = Directory(inet_org_person_schema(),
                          cache=cache,
                          start_tls=True,
                          uri=f"ldap://{LDAP_HOST}:{int(LDAP_PORT)}",
                          simple_bind=True,
                          bind_dn=BIND_DN,
                          bind_password=BIND_PASSWORD)
    directory.provider.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
    return directory


def validate_attributes(expected_dn, actual_dn, entry):
    # see the LDIF at src/test/docker/ldif/9-people.ldif for literal values used here
    assert actual_dn == expected_dn
    assert entry["objectClass"] == "inetOrgPerson"
    assert entry["cn"] == "Jordan Fletcher"
    assert entry["sn"] == "Fletcher"
    assert entry["givenName"] == "Jordan"


def test_fetch_one_scope_base(directory):
    expected_dn = DistinguishedName("uid=fletcher,dc=example,dc=org")
    actual_dn, entry = directory.fetch_one(expected_dn, Scope.BASE)
    directory.cache.cache_listener.on_search_miss.assert_called_once_with(expected_dn, Scope.BASE, None)
    validate_attributes(expected_dn, actual_dn, entry)

    # After the first search using the BASE scope, a subsequent search with the same params should
    # be found in the entry cache
    actual_dn, entry = directory.fetch_one(expected_dn, Scope.BASE)
    captor = Captor(Dict())
    directory.cache.cache_listener.on_entry_hit.assert_called_once_with(expected_dn, captor)

    assert captor.value["objectClass"][0] == entry["objectClass"]
    assert captor.value["uid"][0] == actual_dn["uid"]


def test_fetch_one_scope_subtree(directory):
    # see the LDIF at src/test/docker/ldif/9-people.ldif for literal values

    expected_uid = "fletcher"
    base = DistinguishedName("dc=example,dc=org")
    expected_dn = DistinguishedName(f"uid={expected_uid},{base}")
    search_filter = f"uid={expected_uid}"
    actual_dn, entry = directory.fetch_one(base, Scope.SUBTREE, search_filter)
    directory.cache.cache_listener.on_search_miss.assert_called_once_with(base, Scope.SUBTREE, search_filter)
    validate_attributes(expected_dn, actual_dn, entry)

    # After the first search using a scope other than BASE, a subsequent search with the
    # same params should be found in the search cache
    directory.fetch_one(base, Scope.SUBTREE, search_filter)
    captor = Captor(List())
    directory.cache.cache_listener.on_search_hit.assert_called_once_with(base, Scope.SUBTREE, search_filter, captor)

    assert len(captor.value) == 1
    assert captor.value[0] == expected_dn


def test_fetch_all(directory):
    # see the LDIF at src/test/docker/ldif/9-people.ldif for literal values

    expected_uid = "fletcher"
    base = DistinguishedName("dc=example,dc=org")
    expected_dn = DistinguishedName(f"uid={expected_uid},{base}")
    search_filter = f"uid={expected_uid}"
    actual_results = directory.fetch_all(base, Scope.SUBTREE, search_filter)
    directory.cache.cache_listener.on_search_miss.assert_called_once_with(base, Scope.SUBTREE, search_filter)
    assert len(actual_results) == 1
    actual_result = actual_results[0]
    validate_attributes(expected_dn, actual_result[0], actual_result[1])

    # After the first search using the a scope other than BASE, a subsequent search with the
    # same params should be found in the search cache
    directory.fetch_all(base, Scope.SUBTREE, search_filter)
    captor = Captor(List())
    directory.cache.cache_listener.on_search_hit.assert_called_once_with(base, Scope.SUBTREE, search_filter, captor)

    assert len(captor.value) == 1
    assert captor.value[0] == expected_dn


def test_fetch_all_cached_search(directory):
    # A positive search result should populate the cache with the matching entries as
    # well as caching the search itself with the search parameters and corresponding list
    # of matching DNs.
    #
    # see the LDIF at src/test/docker/ldif/9-people.ldif for literal values

    expected_uid = "fletcher"
    base = DistinguishedName("dc=example,dc=org")
    expected_dn = DistinguishedName(f"uid={expected_uid},{base}")
    search_filter = f"uid={expected_uid}"

    # Fill the cache with the search result
    directory.fetch_all(base, Scope.SUBTREE, search_filter)
    directory.cache.cache_listener.on_search_miss.assert_called_once_with(base, Scope.SUBTREE, search_filter)

    # This fetch should come from the cache
    actual_results = directory.fetch_all(base, Scope.SUBTREE, search_filter)
    captor = Captor(List())
    directory.cache.cache_listener.on_search_hit.assert_called_once_with(base, Scope.SUBTREE, search_filter, captor)

    assert len(actual_results) == 1
    actual_result = actual_results[0]
    validate_attributes(expected_dn, actual_result[0], actual_result[1])

    assert len(captor.value) == 1
    assert captor.value[0] == expected_dn

    # Fetching any of the entries in the search result using Scope.BASE and no filter expression
    # should be satisfied by the cache.
    expected_dn = actual_result[0]
    expected_entry = actual_result[1]
    actual_result = directory.fetch_one(expected_dn, Scope.BASE)
    directory.cache.cache_listener.on_entry_hit.assert_called_with(expected_dn, expected_entry.attrs())
    assert actual_result[0] == expected_dn
    assert actual_result[1] == expected_entry


def test_fetch_all_cached_negative_result(directory):
    # A negative search result is an empty list of matching DNs. The cache must take care to
    # distinguish a negative result from a cache miss, so that we avoid repeatedly performing
    # a search that returns no results. This test case performs a search that returns an empty
    # result list and confirms that a subsequent search with the same parameter is correctly
    # handled by the cache with no additional directory search.
    #
    # ee the LDIF at src/test/docker/ldif/9-people.ldif for literal values used here.

    expected_uid = "DOES_NOT_EXIST"
    base = DistinguishedName("dc=example,dc=org")
    expected_dn = DistinguishedName(f"uid={expected_uid},{base}")
    search_filter = f"uid={expected_uid}"

    # Fill the cache with the search result (which should be empty)
    directory.fetch_all(base, Scope.SUBTREE, search_filter)
    directory.cache.cache_listener.on_search_miss.assert_called_once_with(base, Scope.SUBTREE, search_filter)
    ttl_captor = Captor(Int())
    directory.cache.cache_listener.on_search_fill.assert_called_once_with(base, Scope.SUBTREE, search_filter,
                                                                          [], ttl_captor)
    assert ttl_captor.value == directory.cache.search_ttl

    # This fetch should come from the cache
    actual_results = directory.fetch_all(base, Scope.SUBTREE, search_filter)
    results_captor = Captor(List())
    directory.cache.cache_listener.on_search_hit.assert_called_once_with(base, Scope.SUBTREE, search_filter,
                                                                         results_captor)

    assert len(actual_results) == 0
    assert len(results_captor.value) == 0

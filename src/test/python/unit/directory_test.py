from unittest.mock import Mock

from pytest import fixture, raises

from ldapter import Attribute, Directory, DistinguishedName, ObjectClass, Schema, Scope, TooManyResultsError

URI = "some-uri"
SEARCH_DN = DistinguishedName("cn=somebody,dc=example,dc=org")
SEARCH_FILTER = "some-search-filter"
BIND_DN = DistinguishedName("cn=admin")
BIND_PASSWORD = "secret"
OBJECT_CLASS = "objectClass"
OBJECT_CLASS_NAME = "someObjectClass"
ATTRIBUTE_NAME = "someAttribute"
ATTRIBUTE_VALUE = "someValue"


@fixture
def schema():
    return Schema(ObjectClass(OBJECT_CLASS_NAME, Attribute(OBJECT_CLASS), Attribute(ATTRIBUTE_NAME)))


@fixture
def directory(schema):
    directory = Directory(uri=URI, start_tls=False,
                          schema=schema, provider=Mock(), cache=Mock(),
                          simple_bind=True, bind_dn=BIND_DN, bind_password=BIND_PASSWORD)
    directory.provider.is_connected = Mock(return_value=True)
    return directory


def attrs(attribute_value=ATTRIBUTE_VALUE):
    return {
        "objectClass": [OBJECT_CLASS_NAME],
        ATTRIBUTE_NAME: [attribute_value],
    }


def raw(attrs):
    return {k: [v.encode("UTF-8") for v in a] for k, a in attrs.items()}


def result(dn, attrs):
    return dn, attrs


def test_connect_simple_bind(directory):
    directory.simple_bind = True
    directory.start_tls = False
    directory.provider.is_connected = Mock(return_value=False)
    directory.connect()
    directory.provider.connect.assert_called_once_with(URI)
    directory.provider.bind_simple.assert_called_once_with(BIND_DN, BIND_PASSWORD)


def test_connect_simple_bind_and_tls(directory):
    directory.simple_bind = True
    directory.start_tls = True
    directory.provider.is_connected = Mock(return_value=False)
    directory.connect()
    directory.provider.connect.assert_called_once_with(URI)
    directory.provider.start_tls.assert_called_once()
    directory.provider.bind_simple.assert_called_once_with(BIND_DN, BIND_PASSWORD)


def test_connect_sasl_external_bind(directory):
    directory.simple_bind = False
    directory.start_tls = True
    directory.provider.is_connected = Mock(return_value=False)
    directory.connect()
    directory.provider.connect.assert_called_once_with(URI)
    directory.provider.start_tls.assert_called_once()
    directory.provider.bind_sasl_external.assert_called_once()


def validate_result(actual_result):
    assert actual_result[0] == SEARCH_DN
    actual_entry = actual_result[1]
    assert OBJECT_CLASS in actual_entry
    assert actual_entry[OBJECT_CLASS] == OBJECT_CLASS_NAME
    assert ATTRIBUTE_NAME in actual_entry
    assert actual_entry[ATTRIBUTE_NAME] == ATTRIBUTE_VALUE


def validate_results(actual_results):
    assert len(actual_results) == 1


def test_fetch_all_when_no_cache(directory):
    # When no cache configured, it's just a search, transform, and return
    directory.cache = None
    directory.provider.search = Mock(return_value=[result(str(SEARCH_DN), raw(attrs()))])
    scope = Scope.BASE
    actual_results = directory.fetch_all(SEARCH_DN, scope, SEARCH_FILTER)
    directory.provider.search.assert_called_once_with(SEARCH_DN, scope, SEARCH_FILTER, 0)
    validate_results(actual_results)


def test_fetch_all_scope_base_no_filter_when_entry_cache_hit(directory):
    # If the scope is base and no search filter, should probe the entry cache first
    # and return the cached entry on a hit, without doing a search of the directory.
    directory.cache.get_entry = Mock(return_value=result(SEARCH_DN, attrs()))
    scope = Scope.BASE
    actual_results = directory.fetch_all(SEARCH_DN, scope)
    directory.provider.search.assert_not_called()
    validate_results(actual_results)


def test_fetch_all_when_search_cache_hit(directory):
    # Before searching, should probe the search cache. On a hit, it should return the
    # entries without doing a search of the directory.
    directory.cache.get_search = Mock(return_value=[(SEARCH_DN, attrs())])
    scope = Scope.SUBTREE  # don't use Scope.BASE to avoid probing entry cache first
    actual_results = directory.fetch_all(SEARCH_DN, scope, SEARCH_FILTER)
    directory.provider.search.assert_not_called()
    validate_results(actual_results)


def test_fetch_all_when_search_cache_hit_with_missing_entry(directory):
    # DirectoryCache.get_search can return a tuple with a distinguished name and no attributes
    # dict; i.e a cached directory entry from a search might expire before the search result
    # expires. In this case, the corresponding entry should be re-fetched and put into the cache.
    directory.cache.get_search = Mock(return_value=[(SEARCH_DN, None)])
    directory.provider.search = Mock(return_value=([(str(SEARCH_DN), raw(attrs()))]))
    scope = Scope.SUBTREE  # don't use Scope.BASE to avoid probing entry cache first
    actual_results = directory.fetch_all(SEARCH_DN, scope, SEARCH_FILTER)
    directory.provider.search.assert_called_once_with(SEARCH_DN, Scope.BASE)
    directory.cache.put_entry.assert_called_once_with(SEARCH_DN, attrs())
    validate_results(actual_results)


def test_fetch_all_when_search_cache_miss(directory):
    # On a search cache miss, it should do a search and put the search result into the cache
    directory.cache.get_search = Mock(return_value=None)
    directory.provider.search = Mock(return_value=([(str(SEARCH_DN), raw(attrs()))]))
    scope = Scope.SUBTREE  # don't use Scope.BASE to avoid probing entry cache first
    actual_results = directory.fetch_all(SEARCH_DN, scope, SEARCH_FILTER)
    directory.provider.search.assert_called_once_with(SEARCH_DN, scope, SEARCH_FILTER, 0)
    directory.cache.put_search.assert_called_once_with(SEARCH_DN, scope, SEARCH_FILTER, [(SEARCH_DN, attrs())])
    validate_results(actual_results)


def test_fetch_one_when_no_result(directory):
    directory.cache = None
    directory.provider.search = Mock(return_value=[])
    scope = Scope.SUBTREE
    actual_result = directory.fetch_one(SEARCH_DN, scope, SEARCH_FILTER)
    directory.provider.search.assert_called_once_with(SEARCH_DN, scope, SEARCH_FILTER, 2)
    assert actual_result is None


def test_fetch_one_when_one_result(directory):
    directory.cache = None
    directory.provider.search = Mock(return_value=[(str(SEARCH_DN), raw(attrs()))])
    scope = Scope.SUBTREE
    actual_result = directory.fetch_one(SEARCH_DN, scope, SEARCH_FILTER)
    directory.provider.search.assert_called_once_with(SEARCH_DN, scope, SEARCH_FILTER, 2)
    validate_result(actual_result)


def test_fetch_one_when_more_than_one_result(directory):
    directory.cache = None
    directory.provider.search = Mock(return_value=[
        (str(SEARCH_DN), raw(attrs())), (str(SEARCH_DN), raw(attrs()))])
    scope = Scope.SUBTREE
    with raises(TooManyResultsError):
        directory.fetch_one(SEARCH_DN, scope, SEARCH_FILTER)

    directory.provider.search.assert_called_once_with(SEARCH_DN, scope, SEARCH_FILTER, 2)



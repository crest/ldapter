from datetime import datetime

from pytest import raises

from ldapter import Attribute, DistinguishedName, ObjectClass, Schema


def test_distinguished_name_with_str():
    dn = DistinguishedName("a=b,a=c,b=d")
    assert "a" in dn
    assert dn["a"] == ("b", "c")
    assert "b" in dn
    assert dn["b"] == "d"
    assert "A" in dn
    assert dn["A"] == ("b", "c")
    assert "B" in dn
    assert dn["B"] == "d"


def test_distinguished_name_with_kwargs():
    dn = DistinguishedName(a="a", b="b")
    assert "a" in dn
    assert dn["a"] == "a"
    assert "b" in dn
    assert dn["b"] == "b"
    assert "A" in dn
    assert dn["A"] == "a"
    assert "B" in dn
    assert dn["B"] == "b"


def test_distinguished_name_with_str_and_kwargs():
    with raises(ValueError):
        DistinguishedName("foo=bar", foo="bar")


def test_distinguished_name_eq_and_hash():
    x = DistinguishedName("a=b")
    y = DistinguishedName(a="b")
    z = DistinguishedName("A=B")
    assert x == y == z
    assert hash(x) == hash(y) == hash(z)


def test_distinguished_name_str_and_repr():
    x = DistinguishedName("a=b,c=d")
    y = DistinguishedName(a="b", c="d")
    assert str(x) == str(y)
    assert repr(x) == repr(y)
    assert str(x) != repr(x)
    assert str(x) in repr(x)


def test_distinguished_name_keys():
    dn = DistinguishedName("a=b,c=d")
    assert tuple(dn.keys()) == ("a", "c")


def test_distinguished_name_items():
    dn = DistinguishedName("a=b,c=d")
    assert tuple(dn.items()) == (("a", "b"), ("c", "d"))


def test_attribute_transform_str():
    attr = Attribute("foo")
    assert attr.name == "foo"
    assert attr.multivalued is False
    assert attr.py_type is str
    assert attr.transform(["bar"]) == "bar"
    attr.multivalued = True
    assert attr.transform(["bar"]) == ["bar"]


def test_attribute_transform_int():
    attr = Attribute("foo", int)
    assert attr.transform(["42"]) == 42


def test_attribute_transform_boolean():
    attr = Attribute("foo", bool)
    assert attr.name == "foo"
    assert attr.transform(["true"]) is True
    assert attr.transform(["TRUE"]) is True
    assert attr.transform(["false"]) is False
    assert attr.transform(["FALSE"]) is False
    with raises(ValueError):
        attr.transform(["junk"])


def test_attribute_transform_datetime():
    attr = Attribute("foo", datetime, multivalued=False)
    now = datetime.now()
    assert attr.transform([now.isoformat()]) == now


def test_attribute_transform_dn():
    attr = Attribute("foo", DistinguishedName)
    assert attr.transform(["a=b,c=d"]) == DistinguishedName("a=b,c=d")


def test_object_class():
    attr = Attribute("bar")
    object_class = ObjectClass("foo", attr)
    assert object_class.name == "foo"
    assert attr.name in object_class.attributes
    assert object_class[attr.name] is attr
    assert tuple(object_class.keys()) == (attr.name,)
    assert tuple(object_class.items()) == ((attr.name, attr),)


def test_schema():
    object_class = ObjectClass("foo")
    schema = Schema(object_class)
    assert object_class.name in schema.object_classes
    assert schema.object_classes[object_class.name] is object_class

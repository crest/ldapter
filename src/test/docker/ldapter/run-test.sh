#!/bin/bash

LDAP_PROBE="ldapsearch -H ldap://${LDAP_HOST}:${LDAP_PORT} -D $BIND_DN -w $BIND_PASSWORD -s base -b $PROBE_DN"

echo "waiting for LDAP server"
rc=255
delay=5
count=60
while [ $count -gt 0 -a $rc -ne 0 ]; do
  sleep $delay
  ldapsearch -H ldap://${LDAP_HOST}:${LDAP_PORT} -D $BIND_DN -w $BIND_PASSWORD -s base -b $PROBE_DN > /tmp/probe.out
  rc=$?
  count=$(expr $count - 1)
  if [ $rc -ne  0 ]; then
    echo "LDAP server not ready yet"
  fi
done

if [ $rc -ne 0 ]; then
  echo "LDAP server not responding" >&2
  cat /tmp/probe.out >&2
  exit 1
fi

sleep $delay
echo "LDAP server ready"
. venv/bin/activate
cd /work
python3 -m pytest test/

#!/bin/bash

openssl req -x509 -nodes -days 7300 -newkey rsa:2048 \
    -keyout certs/ldap.key -out certs/ldap.crt -config cert.conf -extensions 'v3_req'